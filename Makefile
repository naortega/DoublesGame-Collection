# Copyright (C) 2017 Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net> All rights reserved.
# Author: Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net>
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
#
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
#
# 3. This notice may not be removed or altered from any source
#    distribution.

# Compilers
ASM=as
CC=gcc
CXX=g++
LD=ld
FTR=gfortran
JAVAC=javac

# Other commands
RM=rm -rf

# Directories
BINDIR=bin

all: x86-64asm c cpp fortran java

x86-64asm:
	mkdir -p $(BINDIR)
	$(ASM) -o src/main-x86_64.o src/main-x86_64.asm
	$(LD) -dynamic-linker /lib64/ld-linux-x86-64.so.2 -o \
		$(BINDIR)/doublesgame-x86_64asm src/main-x86_64.o -lc

c:
	mkdir -p $(BINDIR)
	$(CC) src/main.c -o $(BINDIR)/doublesgame-c

cpp:
	mkdir -p $(BINDIR)
	$(CXX) src/Main.cpp -o $(BINDIR)/doublesgame-cpp

fortran:
	mkdir -p $(BINDIR)
	$(FTR) src/main.f -o $(BINDIR)/doublesgame-f

java:
	mkdir -p $(BINDIR)
	$(JAVAC) src/DoublesGame.java -d $(BINDIR)

.PHONY: clean
clean:
	$(RM) src/*.o
