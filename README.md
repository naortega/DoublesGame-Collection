DoublesGame Collection
======================
This is a collection of sources in different languages for [DoublesGame](https://themusicinnoise.net/programs/doublesgame/). Feel free to contribute your own!

Currently DoublesGame exists in the following languages:
 - Bash
 - C
 - C++
 - Common LISP
 - Fortran
 - Java
 - Lua
 - Perl
 - Python

Building
--------
There's a Makefile you can use to compile all of the sources or simply one of them. Be aware that you will need the compiler for each and the code may be platform/architecture specific depending on the language.

Contributing
------------
Do you have another language? Found a bug? Make merge request or send me a patch at [nortega@themusicinnoise.net](mailto:nortega@themusicinnoise.net) and I'll see what I can do. Just make sure to specify a license and copyright statement at the top of the source file, only free software licenses are accepted.

License
-------
Each file will contain the license given to it by each contributor.
