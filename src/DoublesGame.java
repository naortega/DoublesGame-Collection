/*
 * Copyright (C) 2017 Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net> All rights reserved.
 * Author: Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net>
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 *    distribution.
 */

import java.util.Scanner;

public class DoublesGame {
	private static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		int n = 1;

		while(true) {
			System.out.print("2^" + n + "=");
			int a = Integer.parseInt(in.nextLine());
			if(a == 0) {
				System.out.println("Goodbye!");
				break;
			} else if(a != 1 << n) {
				System.out.println("Wrong answer, try again.");
			} else {
				System.out.println("Correct!");
				++n;
			}
		}
	}
}
