C Copyright (C) 2017 Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net> All rights reserved.
C Author: Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net>
C
C This software is provided 'as-is', without any express or implied
C warranty. In no event will the authors be held liable for any damages
C arising from the use of this software.
C
C Permission is granted to anyone to use this software for any purpose,
C including commercial applications, and to alter it and redistribute it
C freely, subject to the following restrictions:
C
C 1. The origin of this software must not be misrepresented; you must not
C    claim that you wrote the original software. If you use this software
C    in a product, an acknowledgment in the product documentation would be
C    appreciated but is not required.
C
C 2. Altered source versions must be plainly marked as such, and must not be
C    misrepresented as being the original software.
C
C 3. This notice may not be removed or altered from any source
C    distribution.

      program doublesgame
          integer :: a,n = 1
          logical :: run = .TRUE.

          do while(run)
              write(*,*) '2^', n, '='
              read(*,*) a
              if(a .EQ. 0) then
                  write(*,*) "Goodbye!"
                  run = .FALSE.
              elseif(a .NE. 2 ** n) then
                  write(*,*) "Wrong answer, try again."
              else
                  write(*,*) "Correct!"
                  n = n + 1
              endif
          enddo
      end program doublesgame
