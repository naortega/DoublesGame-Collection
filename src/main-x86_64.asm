# Copyright (C) 2017 Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net> All rights reserved.
# Author: Ortega Froysa, Nicolás <deathsbreed@themusicinnoise.net>
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
#
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
#
# 3. This notice may not be removed or altered from any source
#    distribution.
#
.section .data
equation:
	.ascii "2^%d=\0"
query:
	.ascii "%d\0"
goodbye:
	.ascii "Goodbye!\0"
tryagain:
	.ascii "Wrong answer, try again.\0"
correct:
	.ascii "Correct!\0"
number:
	.int 1
answer:
	.int 0

.section .text
	.globl _start

_start:
	mov number, %rsi
	mov $equation, %rdi
	call printf

	mov $query, %rdi
	mov $answer, %rsi
	call scanf
	cmp $0, answer
	je quit

	mov number, %rsi
	shl $1, %rsi
	cmp %rdi, answer
	je next

	mov $tryagain, %rdi
	call puts
	jmp _start

next:
	mov $correct, %rdi
	call puts
	mov number, %rax
	inc %rax
	mov %rax, number
	jmp _start

quit:
	mov $goodbye, %rdi
	call puts
	mov $0, %rdi
	call exit
